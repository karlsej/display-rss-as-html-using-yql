# README #

I'm using this function to replace zRssFeed, which died when Google stopped providing their Feeds API on Dec. 2, 2015.

# How to use #

Include jQuery and the function. Then call it e.g. 

```
#!javascript

newFeed('http://feeds.com/my-feed.xml', 0, 'snippet', 'h3', 'date', '#my-feed');
```

## Arguments ##

* **url**: URL of the feed you want to show
* **maxEntries**: number of entries you want to show; enter 0 to show all
* **snippet**: if you enter 'snippet' here, it will strip out any html and show a brief part of the description. Otherwise it will show the full description (or "encoded" if the feed has that).
* **tag**: the element you want the entry title to be
* **date**: enter "date" to show plainly formatted month/date/year. Enter any other string to not show the date. Would be good to build this out as zRssFeed did. 
* **el**: the selector of the element (div) where you want the feed content to show
* **offset**: enter the number of element to start with; leave undefined to start with the first.

## Notes ##

This won't work with Atom feeds. Looks like Yahoo doesn't normalize the feed structure like Google did.