function newFeed(url, maxEntries, snippet, tag, date, el, offset)
{
    jQuery(el).append('<ul class="feed"></ul>');
    jQuery.getJSON('http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20rss%20where%20url%3D%22' + encodeURIComponent(url) + '%22&format=json&callback=?', function (d)
    {
        var items = jQuery(d.query.results.item);
        if (offset!== undefined) {
           items.splice(0, offset);
        }
        var count = 0;
        //grab ever rss item from the json result request
        items.each(function ()
        {
            //if set up to be infinite or the limit is not reached, keep grabbing items
            if (maxEntries === 0 || maxEntries > count)
            {
                var title = this.title;
                var link = this.link;
                var description;
                if (snippet === 'snippet')
                {
                    description = decodeURIComponent(this.description);
                    description = strip(description);
                    if (description.length > 300) {
                        description = description.substr(0, 280) + '... <a class="rss-more" href="' + link + '">more &rt;&rt;</a>';
                    }
                }
                else if (this.encoded)
                {
                    description = this.encoded;
                }
                else description = this.description;
                var entryDate = new Date(this.pubDate);
                var anItem = '<li class="rssRow"><' + tag + '><a href="' + link + '">' + title + '</a></' + tag + '>';
                if (date === 'date')
                {
                    anItem += '<p>' + entryDate.toLocaleDateString() + '</p>';
                }
                anItem += '<p class="rssDesc">' + description + '</p></li>';
                //append to the div
                jQuery(el).find('ul').append(anItem);
                count++;
            }
        });
    });
}
function strip(html)
{
   var tmp = document.createElement("div");
   tmp.innerHTML = html;
   return tmp.textContent || tmp.innerText || "";
}